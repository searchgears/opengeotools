# coding=UTF-8

import csv

class OpenGeoDbEntry(object):
    __slots__ = ('plz', 'name', 'lat', 'lon', 'ags', 'isCommunityCenter')

    def __str__(self):
        return "(" + ", ".join((self.plz, self.name, self.ags, str(self.lat), str(self.lon))) + ")"


class OpenGeoDbData(object):
    def _addEntry(self, ags, lat, lon, plz, name, isCommunityCenter):
        entry = OpenGeoDbEntry()
        entry.plz = plz
        entry.lat = lat
        entry.lon = lon
        entry.ags = ags
        entry.name = name
        entry.isCommunityCenter = isCommunityCenter
        self.entries.append(entry)

    def _readPlzFile(self, reader):
        for row in reader:
            try:
               self.plzMapping[row[1]] = (float(row[2]), float(row[3]))
            except Exception, e:
                print "Error in line " + str(row)

    def _expandPlzs(self, ags, lat, lon, name, plz):
        lat = float(lat)
        lon = float(lon)
        plzs = filter(lambda x: x != "", plz.split(","))
        if len(plzs) == 1:
            self._addEntry(ags, lat, lon, plzs[0], name, True)
        else:
            self._addEntry(ags, lat, lon, u"", name, True)
            for plz in plzs:
                (plzlon, plzlat) = self.plzMapping.get(plz, (lon, lat))
                self._addEntry(ags, plzlat, plzlon, plz, name, False)

    def _readDeFile(self, reader):
        for row in reader:
            level = row[13].strip()

            if level != "" and (int(level) < 6 or int(level) > 7):
                continue

            plz = row[7].strip()
            ags = row[1].strip()
            name = unicode(row[3].strip(), 'UTF-8')
            lat = row[4].strip()
            lon = row[5].strip()
            if plz != "" and lat != "" and lon != "":
                self._expandPlzs(ags, lat, lon, name, plz)

    def __init__(self, deTab, plzTab):
        self.plzMapping = {}
        self.entries = []
        with open(deTab, 'rb') as deFile, open(plzTab, 'rb') as plzFile:
            reader = csv.reader(plzFile, "excel-tab")
            next(reader, None)
            self._readPlzFile(reader)


            reader = csv.reader(deFile, "excel-tab")
            next(reader, None)
            self._readDeFile(reader)

    def __iter__(self):
        return iter(self.entries)


