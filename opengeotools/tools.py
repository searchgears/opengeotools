import sys
import codecs
import locale
from opengeodb import OpenGeoDbData
from census import CensusData

class LocationsWithInhabitants:
    def __init__(self, plzFile, deFile, censusFile):
        self.opengeodb = OpenGeoDbData(deFile, plzFile)
        self.census = CensusData(censusFile)


    def collectAmbiguousCityNames(self):
        ambiguousNames = {}

        for entry in self.opengeodb:
            ambiguousNames.setdefault(entry.name, set()).add(entry.ags)


        self.ambiguousNames = filter(lambda x: len(x[1]) > 1, ambiguousNames.items())



    def printTsvHeader(self):
        print "%s\t%s\t%s\t%s\t%s\t%s" % ("plz", "name", "lon", "lat", "ewz",  "isCommunityCenter")

    def printTsvEntries(self):
        for entry in self.opengeodb:
            if entry.lat != 0:
                if entry.name in self.ambiguousNames:
                    if entry.ags in self.ambiguousNames[entry.name]:
                        continue

                print locale.format_string("%s\t%s\t%f\t%f\t%s\t%s", (entry.plz, entry.name, entry.lon, entry.lat, self.census.getInhabitants(entry.ags, 0), entry.isCommunityCenter)).encode("UTF-8")

if __name__ == "__main__":
    locale.setlocale(locale.LC_ALL, "de_DE.UTF-8")
    locs = LocationsWithInhabitants(sys.argv[1], sys.argv[2], sys.argv[3])
    locs.collectAmbiguousCityNames()
    locs.printTsvHeader()
    locs.printTsvEntries()