import xlrd, re, locale


BUNDESLAND_NUMBER = re.compile("\d\d", re.UNICODE)

class CensusData(object):

    def __init__(self, censusFile):
        self.ags2inhabitants = {}
        book = xlrd.open_workbook(censusFile, encoding_override="ISO-8859-15")
        sheet = book.sheet_by_index(1)
        for rownum in range(sheet.nrows):
            rows = sheet.row_values(rownum)
            if BUNDESLAND_NUMBER.match(unicode(rows[0])):
                self.ags2inhabitants[rows[2]] = (int(rows[4]), self.unicode(rows[3]), self.unicode(rows[1]))

    def unicode(self, value):
        if isinstance(value, unicode):
            return value
        else:
            return unicode(value, "ISO-8859-15")


    def getInhabitants(self, ags, default=0):
        return self.ags2inhabitants.get(ags, (0, u"", u""))[0]


    def getName(self, ags):
        return self.ags2inhabitants.get(ags, (0, u"", u""))[1]

    def getFederalState(self, ags):
        return self.ags2inhabitants.get(ags, (0, u"", u""))[2]
