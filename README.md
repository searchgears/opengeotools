# About opengeotools

An assorted collection of tools for creating, combining and using publicly available geospatial data.

The main focus is on German geo information.

The project is currently only compatible with Python 2.

# Sources

Statistical census information: https://www.destatis.de/DE/ZahlenFakten/LaenderRegionen/Regionales/Gemeindeverzeichnis/Administrativ/Aktuell/Zensus\_Gemeinden.xls

German cities and geo coordinates: http://fa-technik.adfc.de/code/opengeodb/DE.tab

# Usage

The project can of course be used as a library in order to implement all kinds of functionality regarding the geographical data.

The only use case implemented in terms of the main method is the generation of a list of all German zip code and city combinations along with their number of inhabitants, for instance in order to build a geographical autocomplete feature ranked by city size and including geo coordinates.

python opengeotools/tools.py  PLZ.tab DE.tab Zensus\_Gemeinden.xls

The resulting list is printed to standard out as tab separated data in UTF-8.
