# coding=UTF-8

import unittest
from opengeotools.census import CensusData
from opengeotools.opengeodb import OpenGeoDbData


class ZensusDataTest(unittest.TestCase):

    def setUp(self):
        self.censusData = CensusData("Zensus_Gemeinden_Test.xlsx")

    def testGetInhabitants(self):
        self.assertEqual(210679, self.censusData.getInhabitants("01003000"))
        self.assertEqual(0, self.censusData.getInhabitants("0815"))

    def testGetCensusName(self):
        self.assertEqual("Arkebek", self.censusData.getName("01051002"))


    def testGetFederalState(self):
        self.assertEqual("Schleswig-Holstein", self.censusData.getFederalState("01051002"))


class OpenGeoDbDataTest(unittest.TestCase):

    def setUp(self):
        self.openGeoData = OpenGeoDbData("DE_Test.tab", "PLZ_Test.tab")

    def testGetEntryForZipCodeWithSeveralCities(self):
        all_entries_for_24257 = filter(lambda x : x.plz == "24257", self.openGeoData)
        self.assertEqual(3, len(all_entries_for_24257))
        for entry in all_entries_for_24257:
            self.assertEqual("01057029", entry.ags)
            self.assertIn(entry.name, ["Hohenfelde bei Kiel", "Grünberg bei Kiel",  "Malmsteg bei Kiel"])



    def testGetEntryForCityWithSeveralZipCodes(self):
        all_entries_for_kiel = self._getCity("Kiel")
        self.assertEqual(18, len(all_entries_for_kiel))
        for entry in all_entries_for_kiel:
            if entry.plz == "24103":
                self.assertAlmostEqual(10.1309590421533, entry.lon, 4)
                self.assertAlmostEqual(54.3257079732801, entry.lat, 4)


    def testAgsCenterForCityWithSeveralZipCodes(self):
        all_entries_for_kiel = self._getCity("Kiel")

        found = 0
        for entry in all_entries_for_kiel:
            if entry.plz == "":
                self.assertTrue(entry.isCommunityCenter)
                found += 1
            else:
                self.assertFalse(entry.isCommunityCenter)

        self.assertEqual(1, found)

    def _getCity(self, city):
        cityEntries = filter(lambda x: x.name == city, self.openGeoData)
        return cityEntries

    def testAgsCenterForCommunityWithOneZipCode(self):
        reesdorf = self._getCity("Reesdorf bei Kiel")
        self.assertEqual(1, len(reesdorf))
        self.assertTrue(reesdorf[0].isCommunityCenter)


    def testZipCodeFieldWithTrailingComma(self):
        guenthersdorf = self._getCity("Günthersdorf bei Merseburg")
        self.assertEqual(1, len(guenthersdorf))
        self.assertEqual("06237", guenthersdorf[0].plz)


if __name__ == "__main__":
    unittest.main()